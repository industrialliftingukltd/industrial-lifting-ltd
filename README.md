Industrial Lifting have been supplying the lift and construction industry for over 17 years, providing a quality & reliable service throughout the UK. The lifting and handling equipment can be used in the manufacturing & service industry. We're conveniently located in Staffordshire to service the UK.

Address: Units 1, 2 & 3A, Cinderhill Industrial Estate, Stoke-on-Trent, Staffordshire ST3 5LB, United Kingdom

Phone: +44 1782 595945

Website: [https://ilgukltd.com](https://ilgukltd.com)
